import { Component, OnInit } from '@angular/core';
import {  FileUploadService } from '../file-upload.service';
import { BrowserModule } from '@angular/platform-browser';
import { Employee } from '../interfaces/employee';

@Component({
  selector: 'app-fileupload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})

export class FileuploadComponent implements OnInit {

  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title = 's3-file-uploader-app';
  employee:Employee[];
  similarity:number;
  
  constructor(private fileUploadService: FileUploadService) {
    this.errorMsg = false;
  }

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  refresh(): void {
    this.employee=null;
   }

  onFilePicked(event: Event): void {

    this.errorMsg = false;
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
  }
  
  onFileUpload() {

    if (!this.fileObj) {
      this.errorMsg = true;
      return
    }
    
    const fileForm = new FormData();
    fileForm.append('file', this.fileObj);
    this.fileUploadService.fileUpload(fileForm).subscribe(res => {
      
      {res.similarity?
        res.similarity=parseFloat(res.similarity).toFixed(3)
        :
        null
      }
      this.employee=res;
      console.log(this.employee)

      this.employee=res;
      // console.log(res);
      console.log(this.employee);
     
      this.fileUrl = res['image'];
    });


    
  }
  
}