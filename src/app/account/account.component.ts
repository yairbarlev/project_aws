import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { EmployeeService } from '../employee.service';
import { Employee } from '../interfaces/employee';
import { Time } from '@angular/common';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sale } from './../interfaces/sale';
import { SalesService } from './../sales.service';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
 
})
export class AccountComponent implements OnInit {
   //books;

   employees$; 
   fakeAnswer = "Dying to Leave";
   employees:Employee[];
   userId:string; 
   editstate = [];
   addBookFormOPen = false;

   add(employee:Employee){
    this.employeeService.addEmployee(this.userId,employee.gender,  
      employee.firstName,
      employee.lastName,
      employee.rExperience, 
      employee.educationLevel,
      employee.enrollment,
      employee.majorDiscipline, 
      employee.companySize,
      employee.companytype, 
      employee.educationYears,
      employee.experienceYears,
      employee.yearsInCurrentJob,
      employee.trainingHours,
      employee.city,
      employee.street, 
      employee.state,
      employee.age,
      this.fakeAnswer); 
  }
  constructor(private employeeService:EmployeeService, public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        // this.employees$ = this.employeeService.getEmployees(this.userId); 
        this.employees$ = this.employeeService.getEmployees(this.userId); 
        
        this.employees$.subscribe(
          docs =>{
            console.log('init worked');            
            this.employees = [];
            for(let document of docs){
              const employee:Employee = document.payload.doc.data();
              employee.id = document.payload.doc.id; 
              console.log(employee.id);  
              this.employees.push(employee); 
            }
          }
        ) 
      
 
      }
    )
    } 
 
  }


