import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  answer:string;
  id:string;
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.answer = this.route.snapshot.params.answer; 
    this.id = this.route.snapshot.params.id; 
  }

}
